# turbine

I created this basic barebones template for easily implementing quick sites. It simply serves it's purpose by including all the dependencies that I need, allowing me to get up and go quickly.

It uses the following:

- [roots](https://github.com/jescalan/roots)
- [roots-dynamic-content](https://github.com/carrot/roots-dynamic-content)
- [jeet](https://github.com/mojotech/jeet)

For hosting I recommend [Netlify](https://www.netlify.com/)

Feel free to clone and have a play around if you wish. Follow the setup instructions below.

### Setup

- make sure [node.js](http://nodejs.org) and [roots](http://roots.cx) are installed
- clone this repo down and `cd` into the folder
- open in your favourite text editor
- delete .git folder
- in package.json change "name": "turbine" to your liking "...."
- run `$ npm install`
- run `$ roots watch`

If choosing to host on **Netlify** run `$ npm install roots --save` This command inserts roots into the dependencies of your package.json file, which tells Netlify what tools it needs to build your site.
